

import React from 'react';
import { Container, Tab, Row, Col, Nav, CardDeck, Card } from 'react-bootstrap';
import './Shablon-style.css';





function ContentCol() {

    const BlueColor = {
    color: 'rgba(46, 39,209, 1)'
  }



  return (
       <>

      
    <Container className="mt-2 mb-3">
         <Tab.Container id="left-tabs-example" defaultActiveKey="Clock" >
           <Row>
             <Col sm={2}>
               <Nav variant="pills" className="flex-column mt-2">

                 <Nav.Item>
                   <Nav.Link eventKey="Clock" >Годинники</Nav.Link>
                 </Nav.Item>
                 <Nav.Item>
                   <Nav.Link eventKey="NightLamp" >Світильники</Nav.Link>
                 </Nav.Item>
                 <Nav.Item>
                   <Nav.Link eventKey="Lamp" >Лампи</Nav.Link>
                 </Nav.Item>
                 <Nav.Item>
                   <Nav.Link eventKey="PhotoCollage" >Фотоколажі</Nav.Link>
                 </Nav.Item>
               </Nav>
             </Col>



            {/* column */}
             <Col sm={5} className="column-collapse-content">

             <Tab.Content className="mt-2">
                <Tab.Pane eventKey="Clock">

                <CardDeck >
                    <Card>
                      <Card.Img 
                        variant="top" 
                        className="bg-danger"
                        height={350}
                        />
                      <Card.Body>
                        <Card.Title><strong>Ціна:</strong> <span style={ BlueColor }></span></Card.Title>
                            <Card.Text>
                              <strong>Діаметр:</strong>  
                              <br/><strong>Матеріал:</strong>  
                              <br/><strong>Вробник:</strong> 
                              <br/><strong>ID тоару:</strong> 
                            </Card.Text>
                      </Card.Body>
                    </Card>                  
                  </CardDeck>

                  <CardDeck className="mt-2">
                    <Card>
                      <Card.Img 
                        variant="top" 
                        className="bg-warning"
                        height={350}
                        />
                      <Card.Body>
                        <Card.Title><strong>Ціна:</strong> <span style={ BlueColor }></span></Card.Title>
                            <Card.Text>
                              <strong>Діаметр:</strong>  
                              <br/><strong>Матеріал:</strong> 
                              <br/><strong>Вробник:</strong> 
                              <br/><strong>ID тоару:</strong>
                            </Card.Text>
                      </Card.Body>
                    </Card>                  
                  </CardDeck>

                </Tab.Pane>
              </Tab.Content>
              
            </Col>

          </Row>
        </Tab.Container>
      </Container>

      </>
  )
}

export default ContentCol;