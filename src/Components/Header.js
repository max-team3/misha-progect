import React, { Component } from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button } from 'react-bootstrap';
// import logo from './logo192.png';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { FaClock } from 'react-icons/fa';

import logoLamp from '../assets/Logo/lamp2.jpg';

import Home from '../Pages/Home';
import Clock from '../Pages/Clock';
import NightLamp from '../Pages/NightLamp';
import PhotoCollage from '../Pages/PhotoCollage';
import Lamp from '../Pages/Lamp';
import TestPage from '../Pages/TestPage';
import TestPage2 from '../Pages/TestPage2';
import Order from '../Pages/Order';

export default class Header extends Component {
  render () {
    return (
      <div>
        <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
          <Container>
            {/* logo */}
            <Navbar.Brand href="/">
              {/* <img 
                src={logoLamp}
                height="30"
                width="30"
                className="d-inline-block align-top"
                alt="Logo"
              />  */}
              <FaClock 
                height="30"
                width="30"
                className="d-inline-block align-center mr-2"
                alt="Logo"
              />
              Misha Site
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/">Дім</Nav.Link>
                <Nav.Link href="/Clock">Годинники</Nav.Link>
                <Nav.Link href="/NightLamp">Світильники</Nav.Link>
                <Nav.Link href="/Lamp">Лампи</Nav.Link>
                <Nav.Link href="/PhotoCollage">Фотоколажі</Nav.Link>
                <Nav.Link href="/TestPage">TestPage</Nav.Link>
                <Nav.Link href="/TestPage2">TestPage2</Nav.Link>
                <Nav.Link href="/Order">Замовити</Nav.Link>
              </Nav>
              {/* <Form inline >
                <FormControl 
                  type="text" 
                  placeholder="Search" 
                  className="mr-sm-2" 
                />
              <Button variant="outline-info">Search</Button>
              </Form> */}
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/Clock" component={Clock} />
            <Route exact path="/NightLamp" component={NightLamp} />
            <Route exact path="/PhotoCollage" component={PhotoCollage} />
            <Route exact path="/Lamp" component={Lamp} />
            <Route exact path="/TestPage" component={TestPage} />
            <Route exact path="/TestPage2" component={TestPage2} />
            <Route exact path="/Order" component={Order} />
          </Switch>
        </Router>
      </div>
    )
  }
}