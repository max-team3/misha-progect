import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import ForestImg from '../assets/forest.jpg';
import BridgeImg from '../assets/bridge.jpg';

// images
import ClockImg1 from '../assets/Clock/clock-1.jpg';
import ClockImg12 from '../assets/Clock/clock-12.jpg';
import photoCollagImag2 from '../assets/Сollage/collage-2.jpg';




// export default class CarouselBox extends Component {
//   render() {
//     return (
//       <Carousel >
//         <Carousel.Item>
//           <img 
//             className="d-block w-100" 
//             scr={ ForestImg }
//             alt="Forest"
//           />
//           <Carousel.Caption>
//             <h3>Forest img</h3>
//             <p>loresdvs sdvsdvsdv svsdvsdv sdvsdvsdv sdsdvsdv sdvsdvsd</p>
//           </Carousel.Caption>
//         </Carousel.Item>

//         <Carousel.Item>
//           <img 
//             className="d-block w-100" 
//             scr={ BridgeImg }
//             alt="Bridge"
//           />
//           <Carousel.Caption>
//             <h3>Bridge img</h3>
//             <p>loresdvs sdvsdvsdv svsdvsdv sdvsdvsdv sdsdvsdv sdvsdvsd</p>
//           </Carousel.Caption>
//         </Carousel.Item>

//         <Carousel.Item>
//           <img 
//             className="d-block w-100" 
//             scr={ ForestImg }
//             alt="Forest"
//           />
//           <Carousel.Caption>
//             <h3>Forest img</h3>
//             <p>loresdvs sdvsdvsdv svsdvsdv sdvsdvsdv sdsdvsdv sdvsdvsd</p>
//           </Carousel.Caption>
//         </Carousel.Item>
//       </Carousel>
//     )
//   }
// }

function CarouselBox() {
  const heightCarousel = {
    height: '100vh'
  }

  return (
    <Carousel className="mt-1">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={photoCollagImag2}
          alt="First slide"
          style={ heightCarousel }
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={ClockImg1}
          alt="Second slide"
          style={ heightCarousel }
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={ClockImg12}
          alt="Third slide"
          style={ heightCarousel }
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  )
}

export default CarouselBox; 

