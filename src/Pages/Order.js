import React, { useState } from 'react';
import { Container, Form, Button, Modal } from 'react-bootstrap';
import emailjs from 'emailjs-com';


// export default class Order extends Component {
//   render() {
//     return (
//       <Container style={{ width: '500px' }}>
//         <h1 className="text-center">Order</h1>
//         <Form>
//           <Form.Group contralId="formBasicEmail">
//             <Form.Label>Email Address</Form.Label>
//             <Form.Control type="email" placeholder="Email" />
//             <Form.Text>
//               We'll never share your email with anyone else
//             </Form.Text>
//           </Form.Group>
          
//           <Form.Group contralId="formBasicPassword">
//             <Form.Label>example text area</Form.Label>
//             <Form.Control as="textarea" rows="3" />
//           </Form.Group>

//           <Form.Group contralId="formBasicCheckbox">
//             <Form.Check type="checkbox" label="Check me out" />
//           </Form.Group>

//           <Button variant="primary" type="submit">Submit</Button>

//         </Form>
//       </Container>
//     )
//   }
// }

function Order() {

  // send Email function
  function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('default_service', 'template_pb76bfq', e.target, 'user_bWaeG1kCZCjOZrty2jO9p')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
  }
  // modal window functions and hooks
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // styles for div instractions and about us
  const instactionStyle = {
    backgroundColor: 'rgba(13, 198, 6, 0.9)',
    borderLeft: '10px solid #008000',
  };

  const aboutUsStyle = {
    backgroundColor: 'rgb(245, 83, 83)',
    borderLeft: '10px solid rgb(228, 26, 11)',
  };

  return (

    <Container>
      {/* div Instractions  */}
      <div style={ instactionStyle }  className="mt-3">
        <h2 className="text-center">Інструкція як зробити замовлення</h2>
        <p className="ml-1">
          Заповніть форму нижче, вкажіть своє <strong> ім'я та прізвище, 
           номер свого телефону </strong> для зв'язку, 
          оберіть <strong> id товару </strong> товару, 
          оберіть <strong>кількість одиниць товару, </strong> 
          вкажіть <strong>адрес пошти та № вітділення, </strong>
          також можете залишити <strong>ваш коментар, </strong> після цього
          з вами звяжется менеджер для уточнення замовлення.
        </p>
        <p className="ml-1">
        Якщо ви хочете замовити різні товари вкажіть їхні 
        <strong> id</strong> в полі 
        <strong> Ваш коментар,</strong> або якщо ви хочете замовити кількість одиниць
        товару якої немає в полі, також напишіть це в полі 
        <strong> Ваш коментар </strong> 
        після цього з вами звяжется менеджер для уточнення
        замовлення.
        </p>
      </div>

      {/* div about us */}
      <div style={ aboutUsStyle }>
        <h2 className="ml-1">Про нас</h2>
        <p className="ml-1">
          <strong>Місцезнаходження: </strong> 
          м.Чернівці Калинівський ринок
        </p>
        <p className="ml-1">
          <strong>Телефон:</strong> +380664548458  
          <strong> Михайло</strong>
        </p>
      </div>

      <Form onSubmit={sendEmail} className="mb-4">
        <Form.Group controlId="exampleForm.ControlInput1">
          <Form.Label>Ім'я та Прізвище</Form.Label>
          <Form.Control type="text" placeholder="Ім'я" name="name" />
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlInput2">
          <Form.Label>Телефон</Form.Label>
          <Form.Control type="text" placeholder="Телефон" name="phone" />
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>ID товара</Form.Label>
          <Form.Control as="select" name="idGoods">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlSelect2">
          <Form.Label>Кількість одиниць товара</Form.Label>
          <Form.Control as="select" name="numberOfGoods">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>10</option>
            <option>10+</option>
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Label>Адреса Нової Пошти або Укр.Пошти</Form.Label>
          <Form.Control 
            as="textarea" 
            rows={3} 
            placeholder="Адреса" 
            name="postAdress"
            />
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlTextarea2">
          <Form.Label>Ваш коментар</Form.Label>
          <Form.Control 
          as="textarea" 
          rows={3} 
          placeholder="Ваш коментар" 
          name="message"
          />
        </Form.Group>

        <Button 
          variant="primary" 
          type="submit" 
          onClick={handleShow}
        >
          Замовити
        </Button>
      </Form>

      {/* modal window */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Замовлення прийнято</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Замовлення прийнято найближчим часом менеджер 
          передзвонить вам для уточнення замовлення. 
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleClose}>
            Закрити
          </Button>
        </Modal.Footer>
      </Modal>

    </Container>
  )
}

export default Order;